# -*- coding: utf-8 -*-
import random
import time
from threading import Thread
from autoreg_mail import MailruAutoreg


class MyThread(Thread):
    """
    A threading example
    """

    def __init__(self, name):
        """Инициализация потока"""
        Thread.__init__(self)
        self.name = name


    def run(self):
        """Запуск потока"""
        MailruAutoreg(q).create_account()
        msg = "%s is running" % self.name
        print(msg)


def create_threads(q):
    """
    Создаем группу потоков
    """
    for i in range(q):
        name = "Thread #%s" % (i + 1)
        my_thread = MyThread(name)
        my_thread.start()
    my_thread.join()



if __name__ == "__main__":
    print('Quantity of accounts:', end='')
    q = int(input())
    create_threads(q)
    input()

