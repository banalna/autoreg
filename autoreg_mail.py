from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import os
import zipfile
import time
import random
import string
import os
import datetime

from get_driver import get_chromedriver


def random_word(length, num=True):
    letters = string.ascii_lowercase + string.ascii_uppercase
    word = ''.join(random.choice(letters) for i in range(length - 3))
    if num:
        numbers = '0123456789'
        word += ''.join(random.choice(numbers) for i in range(3))
    return word


class GmailAutoreg:
    def __init__(self, quantity=1):
        self.quantity = quantity
        self.num_proxy = 0

    def create_account(self):
        for i in range(self.quantity):
            first_name = random_word(8, False)
            last_name = random_word(10, False)
            email = random_word(random.randint(10, 29))
            passwd = random_word(10)

            try:
                browser = get_chromedriver(use_proxy=True, i=self.num_proxy)
            except IndexError:
                print("ends of proxy list")
                break

            if 'gmail.txt' not in os.listdir(os.curdir + "/resourse_files/"):
                with open('gmail.txt', 'w', encoding='utf-8') as f:
                    print('file create')

            browser = webdriver.Chrome(executable_path='chromedriver.exe')
            browser.get("https://accounts.google.com/SignUp?hl=ru-RU")

            browser.find_element_by_id('firstName').send_keys(first_name)
            browser.find_element_by_id('lastName').send_keys(last_name)
            email = browser.find_element_by_id("username").send_keys(email)

            browser.find_element_by_name("Passwd").send_keys(passwd)
            browser.find_element_by_name("ConfirmPasswd").send_keys(passwd)

            browser.find_element_by_id("accountDetailsNext").click()
            # TODO!

            with open("resourse_files/gmail.txt", "r+", encoding='utf-8') as f:
                file = f.write(first_name + ';' + last_name + ';' + email + '@gmail.com' + + ';' + passwd)
                print('ok')
                self.num_proxy += 1


class MailruAutoreg:
    def __init__(self, quantity=1):
        self.quantity = quantity
        self.num_proxy = 0

    def create_account(self):

        if 'mailru.txt' not in os.listdir(os.curdir + "/resourse_files/"):
            with open('resourse_files/mailru.txt', 'w', encoding='utf-8') as f:
                print('file create')

        for i in range(self.quantity):
            first_name = random_word(8, False)
            last_name = random_word(10, False)
            email = random_word(random.randint(10, 29))
            passwd = random_word(10)
            day = str(random.randint(1, 28))
            month = str(random.randint(0, 11))
            year = str(random.randint(1930, 2000))
            gender = random.randint(0, 1)

            # try:
            #     browser = get_chromedriver(use_proxy=True, i=self.num_proxy)
            # except IndexError:
            #     print("ends of proxy list")
            #     break
            chrome_options = webdriver.ChromeOptions()
            # chrome_options.add_argument('--proxy-server=%s' % PROXY)
            browser = webdriver.Chrome(chrome_options=chrome_options)

            browser.get("https://account.mail.ru/signup/simple")
            # Email, ФИО
            try:
                browser.find_element_by_name('firstname').send_keys(first_name)
            except:
                continue
            browser.find_element_by_name('lastname').send_keys(last_name)
            browser.find_element_by_class_name('b-email__name').find_element_by_class_name(
                'b-input_disallow-custom-domain ').send_keys(email)

            # Пароль
            browser.find_element_by_name('password').send_keys(passwd)
            time.sleep(5)
            browser.find_element_by_id('passwordRetry').send_keys(passwd)
            # browser.find_element_by_name("ConfirmPasswd").send_keys(passwd)

            # Дата (день, месяц, год)
            browser.find_element_by_class_name('b-date__day').find_element_by_class_name(
                'b-dropdown__ctrl__text').click()
            browser.find_element_by_class_name('day' + day).find_element_by_class_name(
                'b-dropdown__list__item__text').click()

            browser.find_element_by_class_name('b-date__month').find_element_by_class_name(
                'b-dropdown__ctrl__text').click()
            browser.find_element_by_class_name('b-date__month').find_element_by_xpath(
                f'.//*[@data-num={month}]/span').click()

            browser.find_element_by_class_name('b-date__year').find_element_by_class_name(
                'b-dropdown__ctrl__text').click()
            browser.find_element_by_class_name('b-date__year').find_element_by_xpath(
                f'.//*[@data-num={str(datetime.datetime.now().year - int(year))}]/span').click()

            # Пол
            if gender:
                browser.find_element_by_class_name('b-radiogroup').find_element_by_xpath(
                    './/*[@data-mnemo="sex-male"]').click()
            else:
                browser.find_element_by_class_name('b-radiogroup').find_element_by_xpath(
                    './/*[@data-mnemo="sex-female"]').click()

            browser.find_element_by_class_name('btn_main').click()

            try:
                time.sleep(0.2)
                browser.find_element_by_class_name('b-cellphone')
                print('Need a phone')
                break
            except WebDriverException:
                self.num_proxy += 1
                with open('resourse_files/mailru.txt', "a", encoding='utf-8') as f:
                    f.write(email + '@mail.ru' + ';' + passwd + '\n')
                    # email + '@mail.ru' + ';' + passwd + ';' + first_name + ';' + last_name + ';' + ';' + str(gender)
                    # + day + ';' + month + ';' + year + '\n')
                    print('Writed to file!')


class YandexAutoreg:
    def __init__(self, quantity=1):
        self.quantity = quantity
        self.num_proxy = 0

    def create_account(self):

        if 'yandex.txt' not in os.listdir(os.curdir + "/resourse_files/"):
            with open('resourse_files/yandex.txt', 'w', encoding='utf-8') as f:
                print('file create')

        for i in range(self.quantity):
            first_name = random_word(8, False)
            last_name = random_word(10, False)
            email = random_word(random.randint(10, 29))
            passwd = random_word(10)
            day = str(random.randint(1, 28))
            month = str(random.randint(0, 11))
            year = str(random.randint(1930, 2000))
            gender = random.randint(0, 1)

            # try:
            #     browser = get_chromedriver(use_proxy=True, i=self.num_proxy)
            # except IndexError:
            #     print("ends of proxy list")
            #     break
            chrome_options = webdriver.ChromeOptions()
            # chrome_options.add_argument('--proxy-server=%s' % PROXY)
            browser = webdriver.Chrome(chrome_options=chrome_options)

            browser.get("https://passport.yandex.ua/registration")
            # TODO YANDEX REGISTRATION
            # Email, ФИО
            try:
                browser.find_element_by_name('firstname').send_keys(first_name)
            except:
                continue
            browser.find_element_by_name('lastname').send_keys(last_name)
            browser.find_element_by_class_name('b-email__name').find_element_by_class_name(
                'b-input_disallow-custom-domain ').send_keys(email)

            # Пароль
            browser.find_element_by_name('password').send_keys(passwd)
            time.sleep(5)
            browser.find_element_by_id('passwordRetry').send_keys(passwd)
            # browser.find_element_by_name("ConfirmPasswd").send_keys(passwd)

            # Дата (день, месяц, год)
            browser.find_element_by_class_name('b-date__day').find_element_by_class_name(
                'b-dropdown__ctrl__text').click()
            browser.find_element_by_class_name('day' + day).find_element_by_class_name(
                'b-dropdown__list__item__text').click()

            browser.find_element_by_class_name('b-date__month').find_element_by_class_name(
                'b-dropdown__ctrl__text').click()
            browser.find_element_by_class_name('b-date__month').find_element_by_xpath(
                f'.//*[@data-num={month}]/span').click()

            browser.find_element_by_class_name('b-date__year').find_element_by_class_name(
                'b-dropdown__ctrl__text').click()
            browser.find_element_by_class_name('b-date__year').find_element_by_xpath(
                f'.//*[@data-num={str(datetime.datetime.now().year - int(year))}]/span').click()

            # Пол
            if gender:
                browser.find_element_by_class_name('b-radiogroup').find_element_by_xpath(
                    './/*[@data-mnemo="sex-male"]').click()
            else:
                browser.find_element_by_class_name('b-radiogroup').find_element_by_xpath(
                    './/*[@data-mnemo="sex-female"]').click()

            browser.find_element_by_class_name('btn_main').click()

            try:
                time.sleep(0.2)
                browser.find_element_by_class_name('b-cellphone')
                print('Need a phone')
                break
            except WebDriverException:
                self.num_proxy += 1
                with open('resourse_files/mailru.txt', "a", encoding='utf-8') as f:
                    f.write(email + '@mail.ru' + ';' + passwd + '\n')
                    # email + '@mail.ru' + ';' + passwd + ';' + first_name + ';' + last_name + ';' + ';' + str(gender)
                    # + day + ';' + month + ';' + year + '\n')
                    print('Writed to file!')


if __name__ == "__main__":
    print('Quantity of accounts:', end='')
    q = int(input())
    MailruAutoreg(q).create_account()
    # GmailAutoreg(q).create_account()
    input()
